---
title: Graphql Training

language_tabs: # must be one of https://git.io/vQNgJ
  - javascript

search: true

toc_footers:
  - <a href="/index.html">Graphql Training</a>
  - <a href="/react.html">React Training</a>
---

# Introduction

Graphql can be used to replace REST-based API for data access. Why Graphql is better:

1.  Single URL
2.  Declarative query language (you'll see)
3.  Faster development time
4.  Works with **any** stores - Sequelize, Objection, Redis, other REST API, direct data (array, hash), etc.

REST is not completely replaced as certain usage still better with REST than with Graphql (ex: User authentication).

Please [read more](https://blog.apollographql.com/graphql-vs-rest-5d425123e34b)

## Main Concepts

1.  Query
1.  Retrieve data without altering the data in the source (ex: SELECT)
1.  Mutation
1.  Alter data in the source (ex: DELETE, UPDATE, INSERT)

# Grahpql in Garasilabs

For most parts, we'll use **Apollo Graphql** implementation.

## Server Libraries

```bash
yarn add apollo-server-express@rc
```

The server documentation [Apollo Graphql Server](https://www.apollographql.com/docs/apollo-server/v2/getting-started.html)

## Client Libraries

```bash
yarn add apollo-boost react-apollo graphql-tag graphql
```

The client documentation [Apollo Grahpql Client](https://www.apollographql.com/docs/react/)

## Server Side Code

To run the Graphql service, you'll need to:

1.  Setup with Express
2.  Schema
3.  Resolvers

## Setup with Express

```javascript
const express = require("express");
const bodyParser = require("body-parser");
const { ApolloServer, gql } = require("apollo-server-express");
const schema = require("./schema");
const resolvers = require("./resolver");

const PORT = 3000;

const app = express();

const graphqlServer = new ApolloServer({
  typeDefs: schema,
  resolvers
});

graphqlServer.applyMiddleware({ app });

app.listen({ port: PORT }, () =>
  console.log(
    `🚀 Server ready at http://localhost:${PORT}${graphqlServer.graphqlPath}`
  )
);
```

Setup the `app.js` to run both the Express and Graphql server. Apollo Graphql server is run as a middleware for Express.

We still need to include the **Schema** and **Resolvers**. That's next.

# Grahpql Query

## Schema

```javascript
const typeDefs = `
  type Query {
    movies: [Movie]
    movie(id: Int!): Movie
  }

  type Movie {
    id: Int!
    title: String!
    url: String!
  }
  `;

module.exports = typeDefs;
```

Next is to setup the Schema. Schema requires at least the `Query` type to be defined.

## Resolvers

```javascript
const fakeData = [
  {
    id: 1,
    url: "https://www.imdb.com/title/tt0119177/",
    title: "Gattaca"
  },
  {
    id: 2,
    url: "https://www.imdb.com/title/tt0238380/",
    title: "Equilibrium"
  },
  {
    id: 3,
    url: "https://www.imdb.com/title/tt0118929/",
    title: "Dark City"
  }
];

const resolvers = {
  Query: {
    movies(_, args) {
      return fakeData;
    },
    movie(_, args) {
      return fakeData.filter(item => args.id === item.id).pop();
    }
  }
};

module.exports = resolvers;
```

Resolvers are the implementation of the query that you defined in the **schema.js**. The `fakeData` is included to test the query. Next, we will use real database.

## Testing

```javascript
```

Now use the [Graphql UI](http://localhost:3000/graphql) and run the following query

```graphql
{
  movies {
    id
    title
  }
}
```

```graphql
{
  movie(2) {
    title
  }
}
```

## Using Variables

```graphql
query movie($id: Int!) {
  movie(id: $id) {
    title
    url
  }
}
```

> In the **QUERY VARIABLES** section of the UI (bottom of the page)

```
{
  "id": 1
}
```

Using variables make your query more flexible. Think of it as a function that accepts parameters.

## Client

> First get **node-fetch** to run `fetch` as Node client. If you're using browser, `fetch` is part of the modern browser.

```
yarn add node-fetch
```

> Let's create a Node client to access our data.

```javascript
import fetch from "node-fetch";
import util from "util";

const query = {
  query: `{
      movies {
        id
        title
      }
    }`
};

fetch("http://localhost:3000/graphql", {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json"
  },
  // Send the query as JSON string
  body: JSON.stringify(query)
})
  .then(r => r.json())
  .then(data => console.log("Results: ", util.inspect(data, false, null)));
```

> Access data with **variable**

```javascript
const queryWithVariable = {
  query: `query movie($id:Int!) {
      movie(id:$id) {
        id
        title
      }
    }`,
  // The variable to be passed to the movie query above.
  variables: { id: 2 }
};

fetch("http://localhost:3000/graphql", {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json"
  },
  body: JSON.stringify(queryWithVariable)
})
  .then(r => r.json())
  .then(data => console.log("Results: ", util.inspect(data, false, null)));
```

Since Graphql is using HTTP and POST with JSON body, so any HTTP client libray can be used. You can use JQuery, XMLHttpRequest or more modern option, **fetch**.

# Graphql Mutation

Mutation is changing data at the source.

## Mutation Schema

```javascript
const typeDefs = `
type Query {
  ...
}

type Mutation {
  addMovie(title: String!, url: String!) : Movie
}

type Movie {
  ...
}
`;
```

Schema must use **Mutation** type. Like Query type, Mutation type must also return _something_. Note that `addMovie` returns `Movie`. If for some reason, errors happen then an _exception_ should be thrown.

## Mutation Resolvers

```javascript
const resolvers = {
  Query: {
    ...
  },

  Mutation: {
    addMovie(_, args) {
      // args: {title: "", url: ""}
      const last = lodash.last(fakeData);

      // Adds the id. data is {id: X, title: "", url: ""}
      const data = Object.assign(args, { id: last.id + 1 });
      fakeData.push(data);

      // returns the data
      return data;
    }
  }
};

module.exports = resolvers;
```

The implementation of the resolver is similar with the query. Here's the sample implementation.

# Errors Handling and Generating

```javascript
{
  "data": {
    "movie": "<CONTENTS>"
  },
  "errors": [
    {
    }
  ]
}
```

The Graphql returns value has `data` key, and if there are errors, it'll have `errors` key as well. Always check for the `errors`.

## Generating Error

```javascript
const resolvers = {
  Query: {

    ...

    movie(_, args) {
      const data = fakeData.filter(item => args.id === item.id).pop();
      if (lodash.isEmpty(data)) {
        throw new ApolloError("Not found", "NOT_FOUND");
      }
      return data;
    }
  },
```

To generate errors, there are predefined _Exception_ that you can use `AuthenticationError`, `ForbiddenError` and `UserInputError`. If those don't fit, you can use `ApolloError`.

**Important Note**

To make error checking easier, always define the **CODE**. The `ApolloError(message, code)` API uses second parameter to define the code. Please make sure you code always use all uppercase, ex: "NOT_FOUND", "INSERT_FAIL", "DELETE_FAIL".

[Error documentation](https://www.apollographql.com/docs/apollo-server/v2/features/errors.html)

# Objection.js ORM

**Objection.js** is an Object Relational Model (ORM) similar to Sequelize. Objection also uses 2 other libraries:

1.  [Knex.js](https://knexjs.org/) - for writing SQL in Javascript
2.  [AJV](https://ajv.js.org/) - for data validation

## Setup with Express

```javascript
module.exports = {
  development: {
    client: "postgresql",
    connection: {
      database: "mydb",
      user: "postgres",
      password: "password",
      host: "localhost",
      port: 5432
    },
    pool: {
      min: 10,
      max: 100
    },
    migrations: {
      tableName: "knex_migrations"
    },
    debug: true
  }
};
```

Create `knexfile.js` in the root Express directory with connection to Postgres.

## Create the model

```javascript
const { Model } = require("objection");

class Menu extends Model {
  static get tableName() {
    return "Menus";
  }
}

module.exports = Menu;
```

Assuming there's already a database table called `Menus`, you can create the model out of existing table with the following code. Name the file `models/Menu.js`.

To do query, call the `query` method.

## Query

```javascript
const Menu = require("models/Menu");

Menu.query();
```

> Query with `where`

```javascript
const Menu = require("models/Menu");

// The `where` part is actually Knex
Menu.query().where("id", "=", 2);
```

Objection.js fortunately has pretty good [documentation](https://vincit.github.io/objection.js/). Check them out.

## Using Objection with Graphql

> Given the schema

```javascript
type Query {
  menus(offset: Int!, count: Int!): [Menu]
}
```

> The resolver would be:

```javascript
Query: {
  menus(_, args) {
    return Menu.query()
  }
}
```

To use Objection with Graphql is just like our previous exercise. Set the _schema_ and implement the _resolver_.

Schema and its resolver implementation.

# Authentication

Apollo Server uses `context` to pass authentication/authorization data from HTTP to resolvers. Check the [documentation](https://www.apollographql.com/docs/apollo-server/v2/essentials/data.html) on _Context arguments_.

Sample code from the example:

```javascript
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => ({
    authScope: getScope(req.headers.authorization)
  })
}));

// resolver
(parent, _, context) => {
  if(context.authScope !== ADMIN) throw AuthenticationError('not admin');
  ...
}
```

# Homework

You can work either solo or as a team (of 2). You're free to use Sequelize or Objection, just implement the following and make them accessible by Graphql.

1.  User(id: int, name: string)
2.  Phone(id: int, phoneNumber: string, user_id: int)

User can have many phone numbers. Implement the Graphql to:

1.  Add a new User (Mutation)
2.  Query all users
3.  Add a new phone to a user (Mutation)
4.  Query all users with their phones

> Hint for #3, the _schema_ may look like:

```graphql
mutation {
  addPhoneToUser(userId: Int!, phoneNumber: String!)
}
```

> Hint for #4, the _query_ may look like

```graphql
{
  queryUserWithPhones {
    phones {
      id
      phoneNumber
    }
  }
}
```
