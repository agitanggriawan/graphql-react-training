---
title: React Training

language_tabs: # must be one of https://git.io/vQNgJ
  - javascript

search: true

toc_footers:
  - <a href="/index.html">Graphql Training</a>
  - <a href="/react.html">React Training</a>
---

# Introduction

**React** is used to **build user interfaces**. It's for front-end, not back-end.

## Key Concepts (must remember)

1.  Component
1.  JSX (XML in Javascript)
1.  Properties (Props)
1.  States
1.  Higher Order Component (HOC)
1.  Virtual DOM

React is created in Virtual DOM (so not part of the main DOM yet). When React is used in DOM, it's called **Mount**. Virtual DOM has some algorithm that makes updating the main DOM very fast.

The concepts are best illustrated with examples.

# Setting up

```bash
npm install -g create-react-app

# Create react app
create-react-app my-app
```

Best is to use `create-react-app`.

This will create a basic React app directory. Some key important technologies when a React app is created this way:

1.  **Webpack** this is javascript bundler. More details on [Webpack](https://webpack.js.org/).
2.  **Babel** an ES6/7 transpiler (allows to use new features of Javascript without waiting for browsers compatibilities)
3.  Server to run the app.

# Basic React - Component

```jsx
import React, { Component } from "react";

const App = props => {
  return (
    <div>
      <h1>Body</h1>
    </div>
  );
};

export default App;
```

Let's create the first component. Open `my-app/src/App.js`, remove all of its content.

Then write this code, and run the code with `yarn start`.

Open the url at [http://localhost:3000](http://localhost:3000).

## Mounting a Component

> `index.js` mounts the App component

```jsx
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

// Mounts the main compontent: App
ReactDOM.render(<App />, document.getElementById("root"));
```

Open the `my-app/src/index.js` and also `my-app/public/index.html`.

> `index.html` the DOM element where App component will be mounted.

```html
<html>
...
  <body>
    <div id="root"></div>
  </body>
</html>
```

## JSX

```jsx
const App = props => {
  return (
    <div>
      <h1>Body</h1>
    </div>
  );
};
```

JSX is XML embedded in Javascript. Remember it's **XML** not HTML. XML requires every opening tag `<tag>` to be closed `</tag>` or self closing `<tag />`.

There's a bit more to JSX. Please read more at [JSX](https://reactjs.org/docs/introducing-jsx.html).

## Creating Components

> Create two component and mount them: `<Header>` and `<Footer>`.

```jsx
import React, { Component } from "react";

const Header = props => {
  return (
    <div>
      <h3>Title</h3>
    </div>
  );
};

const Footer = props => {
  return <div>Footer</div>;
};

const App = props => {
  return (
    <div>
      <Header />
      <h1>Body</h1>
      <Footer />
    </div>
  );
};

export default App;
```

The easiest way to create component is using **Functional Component**. This is just a function that returns JSX.

## Properties (Props)

```jsx
const Header = props => {
  return (
    <div>
      <h3>{props.title}</h3>
    </div>
  );
};

const App = props => {
  return (
    <div>
      <Header title="My App" />
      <h1>Body</h1>
    </div>
  );
};
```

You can data into a Component using `props`.

> Component with nested children. It's using `props` still.

```jsx
const Header = props => {
  return (
    <div>
      {/* render the child */}
      <h3>{props.children}</h3>
    </div>
  );
};

const App = props => {
  return (
    <div>
      {/* Header has text element (My App) as child element. */}
      <Header>My App</Header>
      <h1>Body</h1>
    </div>
  );
};
```

## State

```jsx
class App extends React.Component {
  // The statest with initial values
  state = {
    title: "",
    color: "black"
  };

  constructor(props) {
    super(props);
  }

  clickHandler = e => {
    this.setState({ color: "red" });
  };

  render() {
    return (
      <div>
        <Header>
          {/* using the state to color the header */}
          <span style={{ color: this.state.color }}> Hello</span>
        </Header>
        <h1>Body</h1>
        <Footer />
      </div>
    );
  }
}

export default App;
```

Component can be made to remember its **state** (data associated with a mounted component). The **Functional Component** that we wrote so far cannot support state - therefore it's called **Stateless Functional Component**.

To use state, you must convert a functional component to **Class-based Component**. The steps are:

1.  Create an **ES6** `class`, with the same name, that extends `React.Component`.
2.  Add a single empty method to it called `render()`.
3.  Move the body of the function into the `render()` method.
4.  Replace `props` with `this.props` in the `render()` body.
5.  Delete the remaining empty function declaration.

Changing state using `setState` will cause re-render of the component.

Important things to **NOTE**:

1.  `setState` will re-render. Don't set `this.state` directly, use `setState`.
2.  `setState` will merge the state. State with `{a: "1", b: "2"}`, calling `this.setState({b: "3"})` will result in the overall state `{a: "1", b: "3"}`.
3.  Be careful when calling `setState` in render so not to cause render loop.

> **Example:** changing state by calling `setState` on button click. Open the Chrome dev tools and check the console. You'll see the `state` is called twice. Once on first render, second when the button is clicked (re-rendered).

```jsx
class App extends React.Component {
  // the state
  state = {
    title: "",
    color: "black"
  };

  constructor(props) {
    super(props);
  }

  // Use property instead of method
  clickHandler = e => {
    this.setState({ color: "red" });
  };

  // you have to have render method here
  render() {
    return (
      <div>
        <Header>
          <span style={{ color: this.state.color }}> Hello</span>
        </Header>
        <h1>Body</h1>
        <button onClick={this.clickHandler}>Click</button>
        {console.log("State:", this.state)}
        <Footer />
      </div>
    );
  }
}

export default App;
```

# React and Redux

We have looked at React state as away for a component to maintain its status data. Using `setState` to set the state of a component will cause the component to **re-render**.

However, in real app, we would like to allow certain state to affect several components, or an event that happens to a component can affect the UI of other components.

To manage global state, we will use React Redux.

![React Redux](/images/react-redux.svg "Logo Title Text 1")

To use React Redux you need to have the following:

1.  **Action** - Component will send (dispatch) **Action** which contains **Action Name** and **Payload** (data).
2.  **Reducer** - based on the Action dispatched, the **Reducer** will change the states.
3.  **Store** - single global store (takes a reducer) to store global states (there must be only one).

In React Redux, the store will take a reducer. The reducer can set the initial states.

> Install

```
yarn add redux react-redux
```

## Steps in working with React Redux

1.  Create the action (put all the actions in centralized module)
2.  Add the reducer
3.  Connect the component to the store to receive state updates

You can have multiple reducers and many actions (in many files), but there's **only one store**.

It's best illustrated with an example.

## Action

Action is just an object with action **type** and its **payload**.

> Action object

```javascript
{
  type: "SOME_STRING_FOR_TYPE",
  data: <Object | Array | String>
}
```

> Example:

```javascript
{
  type: "SET_COLOR",
  color: "blue"
}
```

Since states is just an object that can be created everywhere, it's best to put them in one place. For example, create a file called `Action.js` and then wrap it in function. So when you need to call an action, you have to call a function that returns an action object.

We'll see later on how to call these action functions.

> **`Action.js`** - put all the actions in one file

```javascript
export const setColor = color => ({
  type: "SET_COLOR",
  color
});

export const setDefaultColor = color => ({
  type: "SET_DEFAULT_COLOR",
  color: "black"
});
```

## Reducer

> **`Reducer.js`**

```javascript
import { combineReducers } from "redux";

// Initial state
const initialState = {
  color: "black",
  others: {
    name: "Joe"
  }
};

// Create the reducer with the initial state.
// Notice: it's a pure function. Never altered the input states, but create a new one instead.
const appColor = (state = initialState, action) => {
  switch (action.type) {
    case "SET_COLOR":
      // return Object.assign({}, { color: action.color }, state);
      return { ...state.myAppReducer, color: action.color };
    case "SET_DEFAULT_COLOR":
      // return Object.assign({}, { color: "black" }, state);
      return { ...state.myAppReducer, color: "black" };
    default:
      return state;
  }
};

export default combineReducers({
  appColor
});
```

Based on the Action received, the reducer will decide on what to change with the states.

To create reducer:

1.  Create a function that takes `state` and `action` parameters. `state` can have initial values (initial states).
2.  Check the `action.type` and change the state.
3.  This reducer function is **pure**, meaning it will never _alter_ or change the current states (no side effect). **It will always return new states**.
4.  Export this reducer to be used in the **store**.

## Set the store

Once you have the reducer, you can set up the **Store**.

In your **index.js** file (this is where you mount your main `<App />` component), set the Redux Higher Order Component to make Redux available throughout the app.

```javascript
// ... other imports

import { createStore } from "redux";
import { Provider } from "react-redux";
import reducer from "./Reducers";
const store = createStore(reducer);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
```

## Connect the components

Connecting the components to store would allow the **states** in the global store to be accessible as **props**. There are 2 things that can be connected as props:

1.  The **state**
2.  The **dispatch**

In this example, `<Header>` would have its component connected so that it can access, and receive upadate (hence can re-render), from the global store.

> **`Header.js`**

```javascript
import React from "react";
import { connect } from "react-redux";

class Header extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        {console.log("PROPS COLOR", this.props.color)}
        <h3 style={{ color: this.props.color }}>{this.props.children}</h3>
        <hr />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  color: state.appColor.color
});

// The result of a "connect()" is a higher order component to wrap the Header component
export default connect(mapStateToProps)(Header);
```

## Sending dispatch

For a component to send an action to the global store to update the state, we can use `dispatch` function. Again, the connected dispatch will be accessible via `props`.

> **`App.js`** - please read the comments in the code

```javascript
// ... rest of imports
import { connect } from "react-redux";
import { setColor, setDefaultColor } from "./Actions";

class App extends React.component {
  clickChangeColor = e => {
    // calling the dispatcher via props
    this.props.setColor("red");
  };

  clickResetColor = e => {
    // calling the dispatcher via props
    this.props.setDefaultColor();
  };

  render() {
    return (
      <div>
        <Header>
          <span>Hello</span>
        </Header>
        <div style={{ color: this.props.color }}>
          <h1>Body</h1>
        </div>
        <button onClick={this.clickChangeColor}>Set Color</button>
        <button onClick={this.clickResetColor}>Back To Default</button>
        <Footer />
      </div>
    );
  }
}

// The state is mapped to props
const mapStateToProps = state => ({
  color: state.appColor.color
});

// The dispatchers are mapped to props
// When called, dispatched the "actions" - setColor or setDefaultColor
const mapDispatchToProps = dispatch => ({
  setColor: color => dispatch(setColor(color)),
  setDefaultColor: () => dispatch(setDefaultColor())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
```

Few things to note here:

1.  A click on a button sends an event
2.  The event is then send the dispatch via `this.props` object (ex: `this.props.setColor()`)
3.  Dispatch will be sent to the store
4.  The store calls the reducer to update the states
5.  Change of states will be sent to the listening component that has connected `mapStateToProps`

## Debugging

A very useful to debug Redux is to see how the actions are being dispatched and what are the resultant states. **redux-logger** will print Redux states to the Chrome console.

> Install `redux-logger`

```
yarn add redux-logger
```

![redux-logger](/images/redux-logger.png "redux-logger in Chrome console")

> Then add them in your `Provider` (in `index.js`) as **middle ware**

```javascript
// ... rest of imports

import { applyMiddleware, createStore } from "redux";
import { Provider } from "react-redux";
import reducer from "./Reducers";
import logger from "redux-logger";

const store = createStore(reducer, applyMiddleware(logger));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
```

## Notes

So far we only send action as object, and they get executed **synchronously**. If for some reasons you need to run action asynchronously (ex: calling AJAX), then you can use various Redux middlewares. One of them is called `redux-thunk` or `redux-saga`.

## References

Very good tutorial of React Redux from Academind. Has 10 videos and easy to follow tutorials.

<iframe width="560" height="315" src="https://www.youtube.com/embed/qrsle5quS7A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
